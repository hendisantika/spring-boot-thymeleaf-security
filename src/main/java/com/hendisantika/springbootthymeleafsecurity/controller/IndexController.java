package com.hendisantika.springbootthymeleafsecurity.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-thymeleaf-security
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 28/04/21
 * Time: 07.40
 */
@Controller
public class IndexController {

    @RequestMapping("")
    private String redirectIndex() {
        return "redirect:products/page";
    }
}
