package com.hendisantika.springbootthymeleafsecurity.controller;

import com.hendisantika.springbootthymeleafsecurity.dto.ProductAddRequest;
import com.hendisantika.springbootthymeleafsecurity.entity.Product;
import com.hendisantika.springbootthymeleafsecurity.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-thymeleaf-security
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 28/04/21
 * Time: 07.42
 */
@Controller
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Product>> getAll() {
        List<Product> products = productService.getAll();
        return ResponseEntity.ok(products);
    }

    @GetMapping("/page")
    public String showProducts() {
        return "products";
    }

    @GetMapping("/page/type")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String showAddUpdateProduct(Model model, @RequestParam("form") String form,
                                       @RequestParam(value = "id", required = false) String id) {
        if (form.equals("addProduct")) {
            model.addAttribute("product", new ProductAddRequest());
            return "add-product";
        } else {
            Optional<Product> currentProduct = productService.findById(Integer.parseInt(id));
            model.addAttribute("product", currentProduct.get());
            return "edit-product";
        }
    }

    @PostMapping
    public String showProducts(Model model, @Valid @ModelAttribute("product") ProductAddRequest productAddRequest,
                               BindingResult result) {
        if (result.hasErrors()) {
            return "add-product";
        }

        productService.addOrUpdateProduct(productAddRequest);
        model.addAttribute("products", productService.getAll());
        return "redirect:products/page";
    }
}
