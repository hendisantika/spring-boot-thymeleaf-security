package com.hendisantika.springbootthymeleafsecurity.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-thymeleaf-security
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 28/04/21
 * Time: 07.41
 */
@Controller
@RequestMapping("/login")
public class LoginController {

    @GetMapping
    public String login(Principal principal) {
        if (principal != null) {
            return "redirect:/products/page";
        }
        return "/login";
    }
}
