package com.hendisantika.springbootthymeleafsecurity.service;

import com.hendisantika.springbootthymeleafsecurity.dto.ProductAddRequest;
import com.hendisantika.springbootthymeleafsecurity.entity.Product;
import com.hendisantika.springbootthymeleafsecurity.exception.DataIsEmptyException;
import com.hendisantika.springbootthymeleafsecurity.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-thymeleaf-security
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 28/04/21
 * Time: 07.23
 */
@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    public List<Product> getAll() {
        List<Product> products = productRepository.findAllProduct();
        if (products.isEmpty()) {
            throw new DataIsEmptyException();
        }
        return products;
    }

    public void addOrUpdateProduct(ProductAddRequest productAddRequest) {
        if (productAddRequest.getIdProduct() == null) {
            Product product = new Product();
            product.setName(productAddRequest.getProductName());
            product.setPrice(new BigDecimal(productAddRequest.getProductPrice()));
            product.setExpireDate(productAddRequest.getProductExpire());
            productRepository.save(product);
        } else {
            Optional<Product> currentProductOpt = productRepository.findById(productAddRequest.getIdProduct());
            Product currentProduct = currentProductOpt.get();
            currentProduct.setName(productAddRequest.getProductName());
            currentProduct.setPrice(new BigDecimal(productAddRequest.getProductPrice()));
            currentProduct.setExpireDate(productAddRequest.getProductExpire());
            productRepository.save(currentProduct);
        }
    }

    public Optional<Product> findById(Integer id) {
        return productRepository.findById(id);
    }

    public void delete(Integer id) {
        productRepository.deleteById(id);
    }
}
