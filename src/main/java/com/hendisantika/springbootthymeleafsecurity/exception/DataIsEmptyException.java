package com.hendisantika.springbootthymeleafsecurity.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-thymeleaf-security
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 28/04/21
 * Time: 07.25
 */
public class DataIsEmptyException extends RuntimeException {
}
