CREATE TABLE `user`
(
    `user_id`    bigint(20) NOT NULL AUTO_INCREMENT,
    `active`     int(11) NOT NULL,
    `email`      varchar(255) NOT NULL,
    `first_name` varchar(255) DEFAULT NULL,
    `last_name`  varchar(255) DEFAULT NULL,
    `password`   varchar(255) NOT NULL,
    `username`   varchar(255) NOT NULL,
    PRIMARY KEY (`user_id`),
    UNIQUE KEY `UK_ob8kqyqqgmefl0aco34akdtpe` (`email`),
    UNIQUE KEY `UK_sb8bbouer5wak8vyiiy4pf2bx` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `hibernate_sequence`
(
    `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Users
-- password in plaintext: "password"
INSERT INTO USER (user_id, password, email, username, first_name, last_name, active)
VALUES (1, '$2a$06$OAPObzhRdRXBCbk7Hj/ot.jY3zPwR8n7/mfLtKIgTzdJa4.6TwsIm', 'user@mail.com', 'admin', 'Hendi', 'Santika',
        1);
-- password in plaintext: "password"
INSERT INTO USER (user_id, password, email, username, first_name, last_name, active)
VALUES (2, '$2a$06$OAPObzhRdRXBCbk7Hj/ot.jY3zPwR8n7/mfLtKIgTzdJa4.6TwsIm', 'johndoe@gmail.com', 'johndoe', 'John',
        'Doe', 1);
