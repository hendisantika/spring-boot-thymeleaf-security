CREATE TABLE `authority`
(
    `id`        int(11) NOT NULL,
    `authority` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;