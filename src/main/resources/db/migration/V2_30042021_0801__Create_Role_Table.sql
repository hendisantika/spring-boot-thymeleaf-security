CREATE TABLE `role`
(
    `role_id` bigint(20) NOT NULL,
    `role`    varchar(255) DEFAULT NULL,
    PRIMARY KEY (`role_id`),
    UNIQUE KEY `UK_bjxn5ii7v7ygwx39et0wawu0q` (`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Roles
INSERT INTO ROLE (role_id, role)
VALUES (1, 'ROLE_ADMIN');
INSERT INTO ROLE (role_id, role)
VALUES (2, 'ROLE_USER');
