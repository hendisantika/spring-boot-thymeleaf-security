CREATE TABLE `product`
(
    `id`          int(11) NOT NULL AUTO_INCREMENT,
    `expire_date` date           DEFAULT NULL,
    `name`        varchar(255)   DEFAULT NULL,
    `price`       decimal(19, 2) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO product(id, expire_date, name, price)
VALUES (1, DATE_ADD(NOW(), INTERVAL 7 DAY), 'Buku Sinar Dunia', 5500),
       (2, DATE_ADD(NOW(), INTERVAL 7 DAY), 'Buku Sinar Akherat', 6500),
       (3, DATE_ADD(NOW(), INTERVAL 7 DAY), 'Buku Sinar Kampung', 7500),
       (4, DATE_ADD(NOW(), INTERVAL 7 DAY), 'Buku Sinar Desa', 8500),
       (5, DATE_ADD(NOW(), INTERVAL 7 DAY), 'Buku Sinar Kecamatan', 9500);