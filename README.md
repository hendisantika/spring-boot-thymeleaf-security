# Spring Boot Thymeleaf Security

### Things todo list

1. Clone this repository: `git clone https://gitlab.com/hendisantika/spring-boot-thymeleaf-security.git`
2. Navigate to the folder `cd spring-boot-thymeleaf-security`
3. Run the application: `mvn clean spring-boot:run`
4. Open your favorite browser: http://localhost:8080

### Images Screen shot

Registration Page

![Registration Page](img/registration.png "Registration Page")

Login Page

![Login Page](img/login.png "Login Page")

Add New Product Page

![Add New Product Page](img/add.png "Add New Product Page")

Product List Page

![Empty Product List Page](img/empty.png "Empty Product List Page")

![Product List Page](img/list.png "Product List Page")